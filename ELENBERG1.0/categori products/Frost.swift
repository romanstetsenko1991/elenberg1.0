//
//  Frost.swift
//  ELENBERG1.0
//
//  Created by Pro on 4/18/19.
//  Copyright © 2019 Stitsuman. All rights reserved.
//

import UIKit

class Frost: NSObject {
    var name = ""
    var image = ""
    var text = ""
    init(name:String, image:String, text: String) {
        self.name = name
        self.image = image
        self.text = text
    }
}
