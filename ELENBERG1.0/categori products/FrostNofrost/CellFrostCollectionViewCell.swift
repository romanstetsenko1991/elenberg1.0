//
//  CellFrostCollectionViewCell.swift
//  ELENBERG1.0
//
//  Created by Pro on 4/14/19.
//  Copyright © 2019 Stitsuman. All rights reserved.
//

import UIKit

class CellFrostCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var frostImage: UIImageView!
    
    @IBOutlet weak var cellText: UILabel!
    @IBOutlet weak var nameproduct: UILabel!
}
