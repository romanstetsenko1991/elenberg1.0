//
//  FrostCollectionViewController.swift
//  ELENBERG1.0
//
//  Created by Pro on 4/14/19.
//  Copyright © 2019 Stitsuman. All rights reserved.
//

import UIKit



class FrostCollectionViewController: UICollectionViewController {

    let frost = [Frost.init(name: "холодильник 51", image: "холодильник 51", text: "Холодильник Elenberg MR-51-O – это однокамерная мини-модель, которая часто используется в гостиницах и пансионатах. Холодильник будет востребован на даче, а также как второй холодильник в доме. Благодаря компактным габаритам для его установки потребуется минимум места. Общий полезный объем модели равен 51 литру."), Frost.init(name: "холодильник 83", image: "холодильник 83", text: "Холодильник Elenberg MR 83-O – создан для того, что бы Ваши продукты хранились долго, и сохраняли свои вкусовые качества. Имея общий объем холодильника в 93 литра, данная модель, отлично подойдет, под домашнее использование, и позволит вместить достаточно продуктов. "), Frost.init(name: "холодильник 146", image: "холодильник 146", text: "Elenberg MRF-146 является хорошим решением для тех, кому необходим холодильник не занимающий много места, но тем не менее с хорошими характеристиками. Так вот высота данного экземпляра составляет 129 см, ширина 47,8 см, а глубина равна 50,2 см."),Frost.init(name: "холодильник 221", image: "холодильник 221", text: "Холодильник 221-O привлекателен внешне, а также обладает небольшими габаритами (Высота – 143 см, Ширина – 54.5 см, Глубина – 56.6 см), что делает его прекрасным выбором для маленькой кухни или же дачи."),Frost.init(name: "холодильник 207", image:"холодильник 207",text: "Если вам нужен холодильник, и при выборе этой техники вы обращаете внимание на надежность и функциональность, но не готовы переплачивать за известный бренд, то холодильник Elenberg MRF 207-O, безусловно, заслуживает вашего внимания. ")]
    override func viewDidLoad() {
        super.viewDidLoad()
    }




    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detail",
            let frostDeteilViewController = segue.destination as? FrostDeteilViewController {
            frostDeteilViewController.frost = sender as? Frost
        }
    }
    

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return frost.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> CellFrostCollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CellFrostCollectionViewCell
            
        cell.nameproduct.text = frost[indexPath.row].name
        cell.frostImage.image = UIImage.init(named: frost[indexPath.row].image)
        
    
        return cell
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "detail", sender: frost[indexPath.row])
     }

}



