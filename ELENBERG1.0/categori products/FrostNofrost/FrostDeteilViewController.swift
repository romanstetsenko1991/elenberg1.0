//
//  FrostDeteilViewController.swift
//  ELENBERG1.0
//
//  Created by Pro on 4/14/19.
//  Copyright © 2019 Stitsuman. All rights reserved.
//

import UIKit

class FrostDeteilViewController: UIViewController {
    @IBOutlet weak var showphoto: UIImageView!
    
    @IBOutlet weak var discription: UILabel!
    var frost: Frost!

    override func viewDidLoad() {
        showphoto.image = UIImage.init(named: frost.image)
        discription.text = frost.text
    }
}
