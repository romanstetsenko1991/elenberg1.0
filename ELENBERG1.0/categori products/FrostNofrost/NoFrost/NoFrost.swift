//
//  NoFrost.swift
//  ELENBERG1.0
//
//  Created by Pro on 4/26/19.
//  Copyright © 2019 Stitsuman. All rights reserved.
//

import UIKit

    class NoFrost: NSObject {
        var name = ""
        var image = ""
        var text = ""
        init(name:String, image:String, text: String) {
            self.name = name
            self.image = image
            self.text = text
        }

}
