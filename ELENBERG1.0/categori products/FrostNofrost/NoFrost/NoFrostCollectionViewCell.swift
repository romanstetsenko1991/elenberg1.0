//
//  NoFrostCollectionViewCell.swift
//  ELENBERG1.0
//
//  Created by Pro on 4/26/19.
//  Copyright © 2019 Stitsuman. All rights reserved.
//

import UIKit

class NoFrostCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var NoFrostText: UILabel!
    @IBOutlet weak var NoFrostImage: UIImageView!
    
}
