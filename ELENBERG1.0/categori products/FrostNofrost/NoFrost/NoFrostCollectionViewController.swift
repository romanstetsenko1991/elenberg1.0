//
//  NoFrostCollectionViewController.swift
//  ELENBERG1.0
//
//  Created by Pro on 4/26/19.
//  Copyright © 2019 Stitsuman. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class NoFrostCollectionViewController: UICollectionViewController {

    var noForst = [NoFrost.init(name:"морозильная камера 85", image: "морозилка 85", text: "Морозильная камера Elenberg FR-85-O – это однокамерная мини-модель, которая часто используется в гостиницах и пансионатах. Морозильник будет востребован на даче, а также как второй в доме. Благодаря компактным габаритам для его установки потребуется минимум места. Общий полезный объем модели равен 93 литр."),NoFrost.init(name:"морозильная камера 186", image: "морозилка 186", text: "Морозильная камера Elenberg FR-186 – это отличный отдельностоящий морозильник, при своем росте в 125 см имеет полезный обьем 140 л, отличный вариант для храния мясных и овощных продуктов"),NoFrost.init(name:"морозильный ларь 201", image: "морозильный ларь 201", text: "Очеь популярная и очень вместительная модель, вмещает порядка 70 единиц дичи"),NoFrost.init(name:"морозильнная камера 143", image: "морозилка 143", text: "Морозилка 143 - лучший вариант для домашнего использования. Имеет 6 полочек и нереально огромный полезный обьем - 170 л") ]

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "noFrostDetail",
            let noFrostDetail = segue.destination as? NoFrostDetailViewController {
                noFrostDetail.noFrost = sender as? NoFrost
            }
       }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {

        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return noForst.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoFrostCell", for: indexPath) as! NoFrostCollectionViewCell

        
        cell.NoFrostText.text = noForst[indexPath.row].name
        cell.NoFrostImage.image = UIImage.init(named: noForst[indexPath.row].image)
    
        return cell
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "noFrostDetail", sender: noForst[indexPath.row])
    }


}
