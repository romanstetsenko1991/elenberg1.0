//
//  NoFrostDetailViewController.swift
//  ELENBERG1.0
//
//  Created by Pro on 4/26/19.
//  Copyright © 2019 Stitsuman. All rights reserved.
//

import UIKit

class NoFrostDetailViewController: UIViewController {

    var noFrost: NoFrost!
    @IBOutlet weak var NoFrostImage: UIImageView!
    
    @IBOutlet weak var NoFrostText: UILabel!
    override func viewDidLoad() {

        NoFrostText.text = noFrost.text
        NoFrostImage.image = UIImage.init(named: noFrost.image)

    }

    
}
