//
//  MicriwaveCollectionViewController.swift
//  ELENBERG1.0
//
//  Created by Pro on 4/28/19.
//  Copyright © 2019 Stitsuman. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class MicriwaveCollectionViewController: UICollectionViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

    }

let microwave = [Microwave.init(name: "ELENBERG 2009", image: "ELENBERG 2009", text: "Самое лучшее сочетание цены и качества, так называема  - первая цена. Имеет рабочий обьем 20 литров, управление - механическое."), Microwave.init(name: "ELENBERG 2090", image: "ELENBERG 2090", text: "Эта микроволновка уже другого уровня.Имеет нержавеющий цвет и электронное управление, так же в ней есть функция гриля.")]

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MicrowaveDetail", let microwaveDeteilViewController = segue.destination as? MicrowaveDetailViewController {
            microwaveDeteilViewController.microwave = sender as? Microwave
        }
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return microwave.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "microwaveCell", for: indexPath) as! MicrowaveCollectionViewCell
    cell.microwaveText.text = microwave[indexPath.row].name
        cell.microwaveImage.image = UIImage.init(named: microwave[indexPath.row].image)

    
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "MicrowaveDetail", sender: microwave[indexPath.row])
    }

}
