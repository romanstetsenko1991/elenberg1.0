//
//  MicrowaveCollectionViewCell.swift
//  ELENBERG1.0
//
//  Created by Pro on 4/28/19.
//  Copyright © 2019 Stitsuman. All rights reserved.
//

import UIKit

class MicrowaveCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var microwaveImage: UIImageView!
    
    @IBOutlet weak var microwaveText: UILabel!
}
