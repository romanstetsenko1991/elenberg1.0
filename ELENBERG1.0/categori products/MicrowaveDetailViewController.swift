//
//  MicrowaveDetailViewController.swift
//  ELENBERG1.0
//
//  Created by Pro on 4/28/19.
//  Copyright © 2019 Stitsuman. All rights reserved.
//

import UIKit

class MicrowaveDetailViewController: UIViewController {

    @IBOutlet weak var microwaveShowImage: UIImageView!
    
    @IBOutlet weak var microwaveDiscription: UILabel!
    
    var microwave : Microwave!
    override func viewDidLoad() {
        super.viewDidLoad()
        microwaveShowImage.image = UIImage.init(named: microwave.image)
        microwaveDiscription.text = microwave.text
 
    }

}
