//
//  Multicooker.swift
//  ELENBERG1.0
//
//  Created by Pro on 4/30/19.
//  Copyright © 2019 Stitsuman. All rights reserved.
//

import UIKit

class Multicooker: NSObject {
    var name = ""
    var image = ""
    var text = ""
    init(name: String, image: String, text: String) {
        self.name = name
        self.image = image
        self.text = text
    }
}
