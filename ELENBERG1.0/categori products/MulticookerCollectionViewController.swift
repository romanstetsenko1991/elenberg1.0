//
//  MulticookerCollectionViewController.swift
//  ELENBERG1.0
//
//  Created by Pro on 4/30/19.
//  Copyright © 2019 Stitsuman. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class MulticookerCollectionViewController: UICollectionViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }

let multicooker = [Multicooker.init(name: "ELENBERG 5012", image: "ELENBERG 5012", text: "Самая простая и универсальная мультиварка. Имеет чашу в 5 литров и стандартных 8 программ. Очеь продаваемя модель благодаря сочетанию цены/качества/функционала."),Multicooker.init(name: "ELENBERG 5521", image: "ELENBERG 5521", text: "Это мультиварка ELENBERG 5521, она классом выше, имеет 42 программы и книгу рецептов от производителя."),Multicooker.init(name: "ELENBERG 5512 D", image: "ELENBERG 5512 D", text: "Это мультиварка с функцией скороварки. Имеет функции автоподогрева и отложеный старт")]
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "multicookerSaugwayDetail",
            let multicookerDetailVC = segue.destination as? MulticookerDetailViewController{
            multicookerDetailVC.multicooker = sender as? Multicooker
        }
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return multicooker.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "multicookerCell", for: indexPath) as! MulticoolerCollectionViewCell
    
        cell.multicookerText.text = multicooker[indexPath.row].name
        cell.multicookerImage.image = UIImage.init(named: multicooker[indexPath.row].image)
    
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "multicookerSaugwayDetail", sender: multicooker[indexPath.row])
    }

}
