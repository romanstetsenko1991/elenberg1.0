//
//  MulticookerDetailViewController.swift
//  ELENBERG1.0
//
//  Created by Pro on 4/30/19.
//  Copyright © 2019 Stitsuman. All rights reserved.
//

import UIKit

class MulticookerDetailViewController: UIViewController {
    var multicooker : Multicooker!
    
    @IBOutlet weak var multicookerDetailImage: UIImageView!
    
    @IBOutlet weak var multicooerDetailText: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

       multicookerDetailImage.image = UIImage.init(named: multicooker.image)
        multicooerDetailText.text = multicooker.text
    }
    



}
