//
//  MulticoolerCollectionViewCell.swift
//  ELENBERG1.0
//
//  Created by Pro on 4/30/19.
//  Copyright © 2019 Stitsuman. All rights reserved.
//

import UIKit

class MulticoolerCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var multicookerImage: UIImageView!
    
    @IBOutlet weak var multicookerText: UILabel!
}
